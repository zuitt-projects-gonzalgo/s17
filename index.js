//common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let marvelHeroes = ["Iron Man", "Captain America", "Thor", "Hulk", "Black Widow", "Halkeye", "Shang Chi", "Spiderman"];
let myTasks = ["drink html", "eat javascript", "inhale css", "bake react"];

//array and indexes
	//[] - array literals
	//reasign values in an array
	console.log(myTasks);
	myTasks[0] = "sleep for 8 hours";
	console.log(myTasks);

	//accessing from an array
	console.log(grades[2]);

	//getting the length of an array
		//array have access to ".length" property to get the numberof elements present in an array
	console.log(marvelHeroes.length)
		//result: 8

	if(marvelHeroes.length > 5) {
		console.log(`We have too many heroes, please contact Thanos`);
	};

	//accessing the last element of an array
	let lastElement = marvelHeroes.length - 1;
	console.log(marvelHeroes[lastElement]);
		//result: spiderman

	//array methods
		//mutator methods
			//these are functions that "mutate" or change an array after they're created

		let fruits = ["apple", "blueberry", "orange", "grapes"];

			//push(element)
				//adds an element in the end of an array and returns the array's length
			console.log(fruits);
			let fruitsLength = fruits.push("mango");
			console.log(fruits);

				//adding multiple elements
				fruits.push("guave", "kiwi");
				console.log(fruits);

			//pop()
				//removes the last element in an array and returns the removed element
			let removedFruit = fruits.pop();
			console.log(removedFruit);
			console.log(fruits);

			fruits.pop();
			console.log(fruits);

			//unshift()
				//add an element/s at the beggining of an array
			fruits.unshift("guava", "kiwi");
			console.log(fruits);

			//shift()
				//removes the first element in array
			fruits.shift();
			console.log(fruits);

			/*
				note:
				push and unshift - adding elements
				pop and shift - removing element
			*/

			//splice()
				//simultaneously removes elements from a specified index number and adds elements
				/*
					syntax:
					arrayName.splice(startingIndex, deleteCount, elementsTobeAdded);
				*/
			fruits.splice(1, 2, "cherry", "watermelon")
			console.log(fruits);

			//1st = starting index: 2nd = deletecount
			fruits.splice(0, 4);
			console.log(fruits);

			fruits.splice(2, 0, "cherry", "buko");
			console.log(fruits);

			//sort()
				//rearranges the array elements in alphanumeric order
				fruits.sort();
				console.log(fruits);

			//reverse()
				//reverses the order of the array elements
				fruits.reverse();
				console.log(fruits);

		//non-mutator methods
			//these functions do not modify or change an array
		let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

			//indexOf()
				//returns the index number of the first matching element found in an array
				//syntax: arrayName.indexOf(searchValue)
			console.log(countries.indexOf("PH"));
				//result: 1
			let indexOfSG = countries.indexOf("SG");
			console.log(indexOfSG);
				//result: 3
			//index that does not exist result to -1
			console.log(countries.indexOf("SK"));
				//result: -1
			console.log(countries.indexOf("JP"));
				//result: -1

			//slice()
				//slices elements from an array and returns a new array
				/*
					syntax:
						arrayName.slice(startingIndex);
						arrayName.slice(sratingIndex, endingIndex);
				*/
				//slice off elements from a specified index to the last element
				console.log(countries.slice(2));
				console.log(countries);

				//slice off elements from a specified index to another index
				console.log(countries.slice(2, 4));

				//slice off elements starting from the last element of an array
				console.log(countries.slice(-3));

			//toString()
				//returns an array as string separated by commas(,)
				//syntax: arrayName.toString();
			console.log(countries.toString());

			let sentence = ["I", "like", "javascript", ".", "It's", "fun", "!"];
			console.log(sentence.toString());

			//concat()
				//combines two arrays and returns a combined result
				//syntax: arrayA.concat(arrayB);
			let tasksArrayA = ["drink html", "eat js"];
			let tasksArrayB = ["inhale css", "breath sass"];
			let tasksArrayC = ["get git", "be node"];

			console.log(tasksArrayA.concat(tasksArrayB));
			
				//combine multiple arrays - separated by comma (,)
				console.log(tasksArrayA.concat(tasksArrayB, tasksArrayC));

				//combining array with elements
				let combined = tasksArrayA.concat("smell express", "throw react");
				console.log(combined);
				console.log(tasksArrayA);

			//join()
				//returns an array as a string separated by a specified separator string
				let members = ["Jisoo", "Jennie", "Rose", "Lisa"];
				console.log(members.join());
					//result: comma as separator
				console.log(members.join(' '));
					//result: space as separtor
				console.log(members.join(""));
					//result: no-space separator

		//iteration methods
			//they're loops designed to perform repetitive tasks on arrays
			//usefull for manipulating array data resulting in complex tasks

			//forEach()
				//have a function inside
				//used when you only want to iterate the values of the array
			let filteredTasks = [];
			countries.forEach(function(task) {
				console.log(task);

				if(task.length > 1) {
					filteredTasks.push(task);
				}
			});

			console.log(filteredTasks);

			console.log(countries);

			//map()
				//iterates on each element and returns new array with different values
				//unlike forEach method, map method required the use of return keyword in order to create another array witith the performed operation
				//use when you want to change the values of the array
			let numbers = [1, 2, 3, 4, 5];
			let numberMap = numbers.map(function(num) {
				return num * num;
			})
			console.log(numberMap);
			
			//every()
				//checks if all elements meet a certain condition
				//uses boolean
				//returns a vue true if all elements meet the condition and false if otherwise
				let allValid = numbers.every(function(num) {
					return num < 4;
				})
				console.log(allValid);
					//result: false

			//some()
				//checks at least one element is the array that meets the condition
				let someValid = numbers.some(function(num) {
					return num < 2;
				})
				console.log(someValid);
					//result: true

			//filter()
				//returns a new array that contains the element that meets a certain condition
				//if there is no elements found, it will return an empty array
				let filterValid = numbers.filter(function(num) {
					return num > 10;
				})
				console.log(filterValid);
					//result: [1, 2]

let colors = ["red", "green", "black", "orange", "yellow"];
let values = ["red", "black", "yellow"];

colors = colors.filter(
	function(item) {
		return values.indexOf(item) === -1;
	}
);
console.log(colors);

			//includes()
				//returns a boolean value true if it finds a matching item in the array
				let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
				let filteredProducts = products.filter(function(product) {
					return product.toLowerCase().includes("a");
				})
				console.log(filteredProducts);

			//multi-dimensional array
				//usefull for storing complex data structures
				let chessboard = [
					["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
					["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
					["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
					["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
					["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
					["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
					["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
					["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]

				]

				console.log(chessboard[1][4]);