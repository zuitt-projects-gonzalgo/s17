let enrolledStudents = ["Leighton Kenny", "Jesse Tanner", "Haley Dillard", "Remi Osborn", "Faheem Todd", "Kaya Blankenship"];

function addStudent(studentName) {
	enrolledStudents.push(studentName);
	console.log(`${studentName} was added to the student's list`);
}

function countStudents() {
	let totalNumber = enrolledStudents.length;
	console.log(`There are a total of ${totalNumber} students enrolled`);
}

function printStudents() {
	enrolledStudents.sort().forEach(student => console.log(student));
}

function findStudent(studentName) {
	let filteredStudents = enrolledStudents.sort().filter(function(student) {
		return student.toLowerCase().includes(studentName);
	})
	let studentNames = filteredStudents.join(', ');

	if(filteredStudents.length === 1) {
		console.log(`${studentNames} is an enrollee.`);
	} else if(filteredStudents.length > 1) {
		console.log(`${studentNames} are enrollees.`);
	} else {
		console.log(`No student found with the name ${studentName}`);
	}
}